package com.glarimy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DirectoryController {
	@Autowired
	private Directory dir;

	@PostMapping("/employee")
	public ResponseEntity<Integer> add(@RequestBody Employee employee) {
		int id = dir.add(employee);
		return new ResponseEntity<Integer>(id, HttpStatus.CREATED);
	}

	@GetMapping("employee/{id}")
	public ResponseEntity<Employee> find(@PathVariable("id") int id) {
		Employee employee = dir.find(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}
}
