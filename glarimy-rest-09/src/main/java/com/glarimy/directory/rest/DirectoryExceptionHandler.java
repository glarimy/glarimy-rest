package com.glarimy.directory.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.glarimy.directory.api.EmployeeNotFoundException;

@ControllerAdvice
public class DirectoryExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler({ RuntimeException.class })
	public ResponseEntity<Error> handle(RuntimeException ex) {
		if (ex instanceof EmployeeNotFoundException)
			return new ResponseEntity<Error>(new Error("Employee not found"), HttpStatus.NOT_FOUND);
		return new ResponseEntity<Error>(new Error("Try later"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
