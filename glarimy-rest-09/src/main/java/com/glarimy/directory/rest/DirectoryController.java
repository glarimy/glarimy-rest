package com.glarimy.directory.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
public class DirectoryController {
	@Autowired
	private Directory dir;

	@GetMapping("/employee/{eid}")
	public ResponseEntity<Employee> find(@PathVariable int eid) {
		return new ResponseEntity<Employee>(dir.find(eid), HttpStatus.OK);
	}
}
