package com.glarimy.directory.domain;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.DirectoryException;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.EmployeeNotFoundException;

@Service
public class SimpleDirectory implements Directory {
	@Autowired
	private RestTemplate template;

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		try {
			Employee result = template.getForObject(new URI("http://localhost:8080/directory/v1/employee/" + eid),Employee.class);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DirectoryException();
		}
	}

}
