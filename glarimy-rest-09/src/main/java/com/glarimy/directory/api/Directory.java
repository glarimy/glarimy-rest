package com.glarimy.directory.api;

public interface Directory {
	Employee find(int eid) throws DirectoryException, EmployeeNotFoundException;
}