package com.glarimy.directory.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.DirectoryException;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.EmployeeNotFoundException;
import com.glarimy.directory.api.InvalidEmployeeException;

public class SimpleDirectory implements Directory {
	private Map<Integer, Employee> employees = new HashMap<Integer, Employee>();

	@Override
	public int add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		if (employee == null)
			throw new InvalidEmployeeException();
		employee.setEid(employees.size());
		employees.put(employee.getEid(), employee);
		return employee.getEid();
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException {
		Employee result = employees.get(eid);
		if (result == null)
			throw new EmployeeNotFoundException();
		return result;
	}

	@Override
	public List<Employee> filter(long phone, int size) throws DirectoryException {
		List<Employee> results = new ArrayList<Employee>();
		for (Employee employee : employees.values())
			if (employee.getPhone() == phone)
				results.add(employee);
		if (results.size() > size)
			return results.subList(0, size);
		return results;
	}

	@Override
	public List<Employee> list(int size) throws DirectoryException {
		List<Employee> results = new ArrayList<Employee>(employees.values());
		if (results.size() > size)
			return results.subList(0, size);
		return results;

	}
}
