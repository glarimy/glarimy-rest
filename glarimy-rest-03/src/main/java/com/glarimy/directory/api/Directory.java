package com.glarimy.directory.api;

import java.util.List;

/**
 * 
 * @author glarimy
 *
 */
public interface Directory {
	/**
	 * 
	 * @param employee
	 * @return
	 * @throws InvalidEmployeeException
	 * @throws DirectoryException
	 */

	int add(Employee employee) throws InvalidEmployeeException, DirectoryException;

	/**
	 * 
	 * @param eid
	 * @return
	 * @throws EmployeeNotFoundException
	 */
	Employee find(int eid) throws EmployeeNotFoundException;

	/**
	 * 
	 * @param phone
	 * @param size
	 * @return
	 * @throws EmployeeNotFoundException
	 */
	List<Employee> filter(long phone, int size) throws EmployeeNotFoundException;

	/**
	 * 
	 * @param size
	 * @return
	 * @throws EmployeeNotFoundException
	 */
	List<Employee> list(int size) throws EmployeeNotFoundException;

}