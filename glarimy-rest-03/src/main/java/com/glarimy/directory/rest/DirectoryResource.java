package com.glarimy.directory.rest;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.domain.SimpleDirectory;

@Path("/directory")
public class DirectoryResource {
	static Directory dir = new SimpleDirectory();

	@GET
	@Path("/employee/{eid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response find(@PathParam("eid") int eid) {
		Employee employee = dir.find(eid);
		return Response.ok().entity(employee).build();
	}

	@GET
	@Path("/employee")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list(@QueryParam("phone") @DefaultValue("0") long phone,
			@HeaderParam("size") @DefaultValue("10") int size) {
		List<Employee> results = null;
		if (phone == 0)
			results = dir.list(size);
		else
			results = dir.filter(phone, size);
		return Response.ok().header("size", size).entity(results).build();
	}

	@POST
	@Path("/employee")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Employee employee) throws Exception {
		int id = dir.add(employee);
		return Response.created(new URI("directory/employee/" + id)).build();

	}
}
