package com.glarimy.directory.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.glarimy.directory.api.DirectoryException;
import com.glarimy.directory.api.EmployeeNotFoundException;
import com.glarimy.directory.api.InvalidEmployeeException;

@Provider
public class DirectoryHandler implements ExceptionMapper<DirectoryException> {

	@Override
	public Response toResponse(DirectoryException exception) {
		if (exception instanceof InvalidEmployeeException) {
			return Response.status(400).build();
		}
		if (exception instanceof EmployeeNotFoundException) {
			return Response.status(404).build();
		}
		return Response.status(500).build();
	}

}
