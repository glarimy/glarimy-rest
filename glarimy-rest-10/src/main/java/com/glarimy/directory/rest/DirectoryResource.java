package com.glarimy.directory.rest;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

@Path("/")
public class DirectoryResource {
	@Autowired
	private Directory dir;

	@GET
	@Path("/employee/{eid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response find(@PathParam("eid") int eid) {
		Employee employee = dir.find(eid);
		
		EmployeeDTO dto = new EmployeeDTO();
		dto.setEmployeeId(employee.getEid());
		dto.setEmployeeName(employee.getName());
		dto.setPhoneNumber(employee.getPhone());
		
		return Response.ok().entity(dto).build();
	}

	@POST
	@Path("/employee")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(EmployeeData employee) throws Exception {
		Employee e = new Employee();
		e.setName(employee.getEmployeeName());
		e.setPhone(employee.getPhoneNumber());
		int id = dir.add(e);
		return Response.created(new URI("/employee/" + id)).build();
	}

}
