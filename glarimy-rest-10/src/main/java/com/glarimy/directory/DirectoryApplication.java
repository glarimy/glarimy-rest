package com.glarimy.directory;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DirectoryApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		new DirectoryApplication().configure(new SpringApplicationBuilder(DirectoryApplication.class)).run(args);
	}

}
