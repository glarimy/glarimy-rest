package com.glarimy.directory;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.glarimy.directory.rest.DirectoryResource;

@Component
public class DirectoryConfig extends ResourceConfig {
	public DirectoryConfig() {
		register(DirectoryResource.class);
	}
}
