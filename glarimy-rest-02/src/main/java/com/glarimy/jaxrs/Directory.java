package com.glarimy.jaxrs;

import java.util.HashMap;
import java.util.Map;

public class Directory {
	private Map<Integer, Employee> employees = new HashMap<Integer, Employee>();
	
	public int add(Employee employee) {
		employee.setEid(employees.size());
		employees.put(employee.getEid(), employee);
		return employee.getEid();
	}
	
	public Employee find(int eid) {
		return employees.get(eid);
	}
}
