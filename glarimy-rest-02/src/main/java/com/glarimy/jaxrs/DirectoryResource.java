package com.glarimy.jaxrs;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/directory")
public class DirectoryResource {
	Directory dir = new Directory();

	@GET
	@Path("/employee/{eid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee find(@PathParam("eid") int eid) {
		Employee employee = dir.find(eid);
		return employee;
	}

	@POST
	@Path("/employee")
	@Consumes(MediaType.APPLICATION_JSON)
	public Integer add(Employee employee) {
		return dir.add(employee);
	}
}
