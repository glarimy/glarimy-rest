- glarimy-rest-01 Publishing REST API with JAX-RS and Jersey Basics
- glarimy-rest-02 Publishing REST API with JAX-RS and Jersey Mappings
- glarimy-rest-03 Publishing REST API with JAX-RS and Jersey Exception Handling
- glarimy-rest-04 Hands-on (Jersey)
- glarimy-rest-05 Publishing REST API with Spring Boot
- glarimy-rest-06 Publishing REST API with Spring Boot and Swagger
- glarimy-rest-07 Publishing REST API with Spring Boot, Swagger, JPA and MySQL
- glarimy-rest-08 Publishing REST API with Spring Boot, Swagger and MongoDB
- glarimy-rest-09 Consuming REST API using Spring Boot
- glarimy-rest-10 Publishing REST API with JAX-RS, Jersey, Spring Boot, JPA and MySQL



