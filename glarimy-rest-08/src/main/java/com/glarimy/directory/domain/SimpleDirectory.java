package com.glarimy.directory.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.DirectoryException;
import com.glarimy.directory.api.Employee;
import com.glarimy.directory.api.EmployeeNotFoundException;
import com.glarimy.directory.api.InvalidEmployeeException;
import com.glarimy.directory.data.EmployeeRepository;

@Service
public class SimpleDirectory implements Directory {
	@Autowired
	EmployeeRepository repo;

	@Override
	public String add(Employee employee) throws InvalidEmployeeException, DirectoryException {
		if (employee == null)
			throw new InvalidEmployeeException();
		Employee result = repo.save(employee);
		return result.getEid();
	}

	@Override
	public Employee find(String eid) throws EmployeeNotFoundException {
		return repo.findById(eid).orElseThrow(() -> new EmployeeNotFoundException());
	}

	@Override
	public List<Employee> filter(long phone, int size) throws DirectoryException {
		return repo.findByPhone(phone);
	}

	@Override
	public List<Employee> list(int size) throws DirectoryException {
		return repo.findAll();
	}
}
