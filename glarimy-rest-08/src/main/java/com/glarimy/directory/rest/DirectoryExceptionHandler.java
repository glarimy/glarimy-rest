package com.glarimy.directory.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.glarimy.directory.api.EmployeeNotFoundException;
import com.glarimy.directory.api.InvalidEmployeeException;

@ControllerAdvice
public class DirectoryExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler({ RuntimeException.class })
	public ResponseEntity<Error> handle(RuntimeException ex) {
		if (ex instanceof EmployeeNotFoundException)
			return new ResponseEntity<Error>(new Error("Not Found"), HttpStatus.NOT_FOUND);
		if (ex instanceof InvalidEmployeeException)
			return new ResponseEntity<Error>(new Error("Employee is not valid"), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<Error>(new Error("Try later"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
