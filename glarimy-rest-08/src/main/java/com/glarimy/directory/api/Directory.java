package com.glarimy.directory.api;

import java.util.List;

public interface Directory {
	String add(Employee employee) throws InvalidEmployeeException, DirectoryException;

	Employee find(String eid) throws EmployeeNotFoundException;

	List<Employee> filter(long phone, int size) throws EmployeeNotFoundException;

	List<Employee> list(int size) throws EmployeeNotFoundException;

}