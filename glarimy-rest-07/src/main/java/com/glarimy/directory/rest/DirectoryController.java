package com.glarimy.directory.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.glarimy.directory.api.Directory;
import com.glarimy.directory.api.Employee;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
public class DirectoryController {
	@Autowired
	private Directory dir;

	@GetMapping("/employee/{eid}")
	public ResponseEntity<Employee> find(@PathVariable("eid") int eid) {
		Employee employee = dir.find(eid);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@GetMapping("/employee")
	public ResponseEntity<List<Employee>> list(@RequestHeader(value = "size", defaultValue = "10") int size,
			@RequestParam(value = "phone") long phone) {
		List<Employee> results = null;
		if (phone == 0)
			results = dir.list(size);
		else
			results = dir.filter(phone, size);

		return new ResponseEntity<List<Employee>>(results, HttpStatus.OK);
	}

	@PostMapping("/employee")
	public ResponseEntity<Integer> add(@RequestBody Employee employee, UriComponentsBuilder builder) throws Exception {
		int id = dir.add(employee);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/employee/{id}").buildAndExpand(id).toUri());
		return new ResponseEntity<Integer>(id, headers, HttpStatus.CREATED);
	}

}
