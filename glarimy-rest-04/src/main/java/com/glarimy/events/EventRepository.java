package com.glarimy.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API: Device: 1. To log new event POST /event Admin: 2. To list all the events
 * (restricted by size) GET /event 3. To stats for 1 hour, 24 hours (by device)
 * GET /device/{did}/stats?window={hour|day} 4. To list all events by device
 * (restricted by size) GET /device/{did}/event
 *
 * URI, Datastructure, Status Code, Headers
 */

public class EventRepository {
	private Map<String, List<Event>> events = new HashMap<String, List<Event>>();

	public void log(Event event) {
		List<Event> entries = events.get(event.getSource());
		if (entries == null)
			entries = new ArrayList<Event>();
		events.put(event.getSource(), entries);
		events.get(event.getSource()).add(event);
	}

	public List<Event> list(String source) {
		return events.get(source);
	}

	public List<Event> list() {
		List<Event> entries = new ArrayList<Event>();
		for (List<Event> list : events.values())
			entries.addAll(list);
		return entries;
	}

	public List<Event> filter(String source, long from, long to) {
		List<Event> results = new ArrayList<Event>();
		for (Collection<Event> entries : events.values())
			for (Event event : entries)
				if (event.getSource().equals(source) && event.getTime() > from && event.getTime() < to)
					results.add(event);
		return results;
	}
}
